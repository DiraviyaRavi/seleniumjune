package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	public static ChromeDriver driver;
	//method for Start Application 
   /*@Given("Start application")	
    public void startApplication() {
		
    driver = new ChromeDriver();
	driver.manage().window().maximize();
    driver.get("http://leaftaps.com/opentaps/control/main");
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);  
    }
	*/
    @Given("Enter the username as Demosalesmanager")
    //method for username 
    public void enterUsername(){
	driver.findElementById("username").sendKeys("Demosalesmanager");
    }

    @Given("Enter the password as crmsfa")
    //method for password
    public void enterThePasswordAsCrmsfa() {
	driver.findElementById("password").sendKeys("crmsfa");
    }

    @When("I click on the login button")
    //method for clicking login button
    public void iClickOnTheLoginButton() {
	driver.findElementByClassName("decorativeSubmit").click();
    }

    @Then("verify login is success")
    //method for successive clicking
    public void verifyLoginIsSuccess() {
	System.out.println("success");
    }

}