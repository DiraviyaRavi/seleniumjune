package fourth;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class ExceptionTry {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		/*try {
			driver.findElementById("username1").sendKeys("DemoSalesManager");
			
		} catch (Exception NoSuchElementException) {
			System.err.println("Username is incorrect");
		}*/
		driver.findElementById("username").sendKeys("DemoSalesManager");
		try {
			driver.close();
			
		} catch (Exception NoSuchSessionException) {
			System.err.println("Can't close browser");
		}
		//try {
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();	
		//} catch (Exception NoSuchElementException) {
			//System.err.println("password is incorrect");
		//} 
		//finally {
			//driver.close();
		//}
		
	}

}
