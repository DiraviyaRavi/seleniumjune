package testCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;

public class PreAndPost {
	
	public static ChromeDriver driver;

	public String[][] driverDetails() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("");
		driver.findElementById("password").sendKeys("");
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@AfterMethod
	public void closeBrowser() {
		driver.close();
	}
	@DataProvider(name="dataSupplier")
	public String[][] getData()
	String[][] data=ReadExcel.getExcelData(excelFileName);
	return data;
}
	}
	
	}


