package second;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Windows {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Window.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementByXPath("//button[text()=\"Open Multiple Windows\"]").click();
		Set<String> windowHandles = driver.getWindowHandles();
		/*for( String sn: windowHandles) {
			driver.switchTo().window(sn);
			String title = driver.getTitle();
			System.out.println(title);
		}
		driver.quit();*/
		List<String> ln= new ArrayList<String>(windowHandles);
		for (int i = 0; i < ln.size(); i++) {
			driver.switchTo().window(ln.get(i));
			String title = driver.getTitle();
			System.out.println(title);
			
		}
		driver.quit();
		
		//shall i use this ?  yh//ok wait

	}

}
