package testCasesTestNG;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class Testcase5MergeLeads{

	
		
		 

			public static void main(String[] args) throws InterruptedException {

				System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverx.exe");
				ChromeDriver driver = new ChromeDriver();
				driver.manage().window().maximize();

				driver.get("http://leaftaps.com/opentaps/control/main");
				driver.findElementById("username").sendKeys("DemoSalesManager");
				driver.findElementById("password").sendKeys("crmsfa");
				driver.findElementByClassName("decorativeSubmit").click();
				driver.findElementByLinkText("CRM/SFA").click();

				driver.findElementByLinkText("Leads").click();

				driver.findElementByLinkText("Merge Leads").click();

				driver.findElementByXPath("//img[@alt=\"Lookup\"][1]").click();
				Set<String> win = driver.getWindowHandles();
				List<String> winName = new ArrayList<String>(win);
				String L = winName.get(1);
				driver.switchTo().window(L);
				Thread.sleep(1000);
				System.out.println(driver.getTitle());

				driver.findElementByXPath("//input[@name=\"firstName\"]").sendKeys("Siva");
				driver.findElementByXPath("//button[@class=\"x-btn-text\"]").click();
				Thread.sleep(3000);
				String ID = driver.findElementByXPath("//table[@class=\"x-grid3-row-table\"]//tr[1]//td[1]").getText();
				System.out.println(ID);
				driver.findElementByXPath("//table[@class=\"x-grid3-row-table\"]//tr[1]//td[1]//a").click();

				L = winName.get(0);
				driver.switchTo().window(L);
				System.out.println(driver.getTitle());

				driver.findElementByXPath("//table[@class=\"twoColumnForm\"]//tr[2]//img").click();
				Set<String> win1 = driver.getWindowHandles();
				List<String> winName1 = new ArrayList<String>(win1);
				String L1 = winName1.get(1);
				driver.switchTo().window(L1);
				Thread.sleep(1000);
				System.out.println(driver.getTitle());

				driver.findElementByXPath("//input[@name=\"firstName\"]").sendKeys("Sarma");
				driver.findElementByXPath("//button[@class=\"x-btn-text\"]").click();
				Thread.sleep(3000);
				String ID1 = driver.findElementByXPath("//table[@class=\"x-grid3-row-table\"]//tr[1]//td[1]").getText();
				System.out.println(ID1);
				driver.findElementByXPath("//table[@class=\"x-grid3-row-table\"]//tr[1]//td[1]//a").click();
				L1 = winName.get(0);
				driver.switchTo().window(L1);
				System.out.println(driver.getTitle());
				
				driver.findElementByClassName("buttonDangerous").click();
				driver.switchTo().alert().accept();
				
				driver.findElementByLinkText("Find Leads").click();
				driver.findElementByXPath("//div[@class=\"x-form-item x-tab-item\"]//div[@class=\"x-form-element\"]//input[@class=\" x-form-text x-form-field\"]").sendKeys("9999");
				driver.findElementByXPath("//button[text()='Find Leads']").click();
				
				Thread.sleep(2000);
				String e = driver.findElementByXPath("//div[@class=\"x-toolbar x-small-editor\"]//div").getText();
				System.out.println("Error shown: "+e);

				Thread.sleep(3000);
				driver.quit();

			}

		}

	


