package testCasesTestNG;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Testcase3Sheet1 {
	@Parameters({"username","password"})
	@Test
	public  void runTestcase3Sheet1(String username, String password) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("TestLeaf");
			driver.findElementById("createLeadForm_firstName").sendKeys("Gopi");
			driver.findElementById("createLeadForm_lastName").sendKeys("J");
			driver.findElementByName("submitButton").click();

		}
}
