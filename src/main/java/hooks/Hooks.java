package hooks;

import org.openqa.selenium.OutputType;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;
import third.PreAndPost1;

public class Hooks extends PreAndPost1  {
	@Before
	public void before(Scenario sc) {
		System.out.println(sc.getName());
		login();
	}
    @After
    public void after(Scenario sc) {
    	if(sc.isFailed()) {
    		byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
    		//embed is used for attaching the screenshot to the html log
    		sc.embed(screenshot, "image/png");
    	}
    	closeBrowser();
    }
    @AfterStep
    public void afterStep(Scenario sc) {
    	byte[] screenshot = driver.getScreenshotAs(OutputType.BYTES);
    }
    
}
