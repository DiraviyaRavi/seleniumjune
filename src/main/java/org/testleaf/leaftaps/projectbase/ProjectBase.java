package org.testleaf.leaftaps.projectbase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ProjectBase {
       public static ChromeDriver driver;
                public static void startApp(String url) {
                	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
            		ChromeDriver driver = new ChromeDriver();
            		driver.get(url);
            		driver.manage().window().maximize();
            		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                }
                public static void closeApp() {
                	driver.close();
                }
                public static void selectByIndex(WebElement element, int index) {
                	Select obj = new Select(element);
                	obj.selectByIndex(index);
                }
}
