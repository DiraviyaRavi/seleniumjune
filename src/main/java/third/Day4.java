package third;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Day4 {

	public static String[][] getExcelData(String excelfileName ) throws IOException {
		//open work book
		@SuppressWarnings("resource")
		XSSFWorkbook wBook= new XSSFWorkbook("C:/Users/Admin/Documents/Workspace/"+excelfileName+".xlsx");
		
		//get to sheet
		XSSFSheet sheet= wBook.getSheetAt(0);
		
		//get row counts
		int rowCount=sheet.getLastRowNum();
		
		//get column count
		int columnCount=sheet.getRow(0).getLastCellNum();
		System.out.println(rowCount+" : "+columnCount);
		String[][] data= new String[rowCount][columnCount];
		for (int i = 1; i <= rowCount; i++) {
			
		//each row
			XSSFRow row=sheet.getRow(i);
			for (int j = 0; j < columnCount; j++) {
				//each column
				
				XSSFCell cell=row.getCell(j);
				
				//get string value
				String value=cell.getStringCellValue();
				//System.out.println(value);
				data[i-1][j]=value;
				
			}
		}
		return data;
	}

}
