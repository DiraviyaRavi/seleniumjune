package third;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestNGannotations {
	@BeforeClass
	public void signup(){
		System.out.println("Beforeclass");
	}
	
	@BeforeMethod
	public void signin(){
		System.out.println("Before method");
	}
	@Test(priority=1)
	public void login(){
		System.out.println("Test ");
		
		
	}
	
	@Test(priority=2 ,dependsOnMethods="login")
	public void login_into(){
		System.out.println("Test into ");
				
	}
	
	@Test(priority=3 )
	public void login_into_1(){
		System.out.println("Test into 1");
				
	}
	
	
	@AfterClass
	public void sign_up(){
		System.out.println("Afterclass");
	}
	
	@AfterMethod
	public void sign_in(){
		System.out.println("After method");
	}

}
