package third;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CommonLogin {
	
	protected ChromeDriver driver;
	@BeforeMethod
	public void login() {
		
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
    driver = new ChromeDriver();
	driver.get("http://leaftaps.com/opentaps/control/main");
	driver.manage().window().maximize();
	WebElement eleUserName = driver.findElementByName("USERNAME");
	eleUserName.sendKeys("DemoSalesManager");
	driver.findElementByName("PASSWORD").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();

}
	@AfterMethod
    public void closeBrowser() {

	driver.close();
}	
}
