$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/features/login.feature");
formatter.feature({
  "name": "Leaftap application",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Login into leaftap",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Start application",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "Enter the username as Demosalesmanager",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.enterUsername()"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "Enter the password as crmsfa",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.enterThePasswordAsCrmsfa()"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "I click on the login button",
  "keyword": "When "
});
formatter.match({
  "location": "LoginSteps.iClickOnTheLoginButton()"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.step({
  "name": "verify login is success",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.verifyLoginIsSuccess()"
});
formatter.result({
  "status": "skipped"
});
formatter.afterstep({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
});